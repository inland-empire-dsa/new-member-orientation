+++
disableToc = false
title = "We Need Comrades"
weight = 15
+++

> Common principles enabled them to discern and name the common enemy — capitalists and landlords promulgating white supremacy and lynch law. Anyone who accepted these principles was a comrade, even when they made mistakes. That they were a comrade meant they were valuable to the struggle. They just needed to be taught, trained. The revolution needs as many recruits as it can get.  

## Read the Article

[We Need Comrades](https://jacobin.com/2019/11/comrades-political-organizing-discipline-jodi-dean)

## Discussion